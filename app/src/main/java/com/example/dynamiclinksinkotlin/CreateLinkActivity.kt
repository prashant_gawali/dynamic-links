package com.example.dynamiclinksinkotlin

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.dynamiclinks.ktx.androidParameters
import com.google.firebase.dynamiclinks.ktx.dynamicLink
import com.google.firebase.dynamiclinks.ktx.dynamicLinks
import com.google.firebase.dynamiclinks.ktx.shortLinkAsync
import com.google.firebase.ktx.Firebase

class CreateLinkActivity : AppCompatActivity() {

    lateinit var refferCode: EditText
    lateinit var dynamicLink: TextView
    lateinit var shortLink: TextView


    lateinit var onShareClick: Button
    lateinit var onCreateLinkClick: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_link)

        onShareClick = findViewById(R.id.btnshareLink)
        onCreateLinkClick = findViewById(R.id.btnGenerateLink)
        refferCode = findViewById<EditText>(R.id.etRefCode)
        dynamicLink = findViewById<EditText>(R.id.tvLink)
        shortLink = findViewById<EditText>(R.id.tvShortLink)


        onShareClick.setOnClickListener {
            onShareLinkClick()
        }
        onCreateLinkClick.setOnClickListener {
            onCreateLinkClick()
        }

    }

    fun onCreateLinkClick() {

        val invitationLink =
            "http://www.yopmail.com/?invitedby=" + refferCode
        val dynamicLink = Firebase.dynamicLinks.dynamicLink {
            link =
                Uri.parse(invitationLink)
            domainUriPrefix = "https://dynamiclinksinkotlin.page.link"
            // Open links with this app on Android
            androidParameters {
                minimumVersion = 1
            }
        }

        val dynamicLinkUri = dynamicLink.uri
        this.dynamicLink.text = dynamicLinkUri.toString()

        val shortLinkTask = Firebase.dynamicLinks.shortLinkAsync {
            link =
                Uri.parse(invitationLink)
            domainUriPrefix = "https://dynamiclinksinkotlin.page.link"

            androidParameters {
                minimumVersion = 1
            }

        }.addOnSuccessListener { result ->

            val shortLink = result.shortLink
            this.shortLink.text = shortLink.toString()
        }.addOnFailureListener {

            Log.d("log_tag", "==> ${it.localizedMessage}", it)
            this.shortLink.text = it.localizedMessage
        }

    }

    fun onShareLinkClick() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, shortLink.text)

        startActivity(Intent.createChooser(intent, "Share Link"))
    }

    fun onCopyLinkClick(type: Int) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        if (type == 0) {
            val clip = ClipData.newPlainText("Link", dynamicLink.text)
            clipboard.setPrimaryClip(clip)
        } else {
            val clip = ClipData.newPlainText("Short Link", shortLink.text)
            clipboard.setPrimaryClip(clip)
        }
    }

}