package com.example.dynamiclinksinkotlin

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.dynamiclinks.ktx.dynamicLinks
import com.google.firebase.ktx.Firebase

class DeepLinkReceiveActivity : AppCompatActivity() {

    lateinit var linkdata: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deep_link_receive)

        linkdata = findViewById<TextView>(R.id.linkReceived).toString()

        Firebase.dynamicLinks
            .getDynamicLink(intent)
            .addOnSuccessListener {
                var deepLink: Uri? = null
                if (it != null) {
                    deepLink = it.link
                    Log.d("TAG", "==> ${deepLink.toString()}")

                    if (deepLink?.getBooleanQueryParameter("invitedby", false) == true)
                        linkdata = deepLink.getQueryParameter("invitedby").toString()
                }
            }
    }
}